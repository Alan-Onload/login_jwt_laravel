<?php

use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::middleware('auth:sanctum')->get('/logado', function (Request $request) {
    return $request->user();
});

Route::apiResource("user", UserController::class);
Route::apiResource("login", LoginController::class);